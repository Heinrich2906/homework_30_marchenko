package com.company.sockets;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heinr on 29.01.2017.
 */
public class MainServer {

    static final List<Socket> clients = new ArrayList<>();
    private static final int PORT_NUMBER = 8888;

    public static void main(String[] args) throws IOException {
        try {
            ServerSocket serverSocket = new ServerSocket(PORT_NUMBER);
            while (true) {
                Socket clientSocket = serverSocket.accept();
                ClientThread clientThread = new ClientThread(clientSocket);
                clientThread.start();
                clients.add(clientSocket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
