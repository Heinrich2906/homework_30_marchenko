package com.company.sockets;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by heinr on 29.01.2017.
 */
public class ClientSendToServer extends Thread {

    private Scanner in = new Scanner(System.in);
    private String userName;
    private Socket socket;

    DataOutputStream dos = null;
    String myText;

    ClientSendToServer(Socket socket, String userName) {
        this.userName = userName;
        this.socket = socket;
    }

    @Override
    public void run() {

        try {
            OutputStream outputStream = socket.getOutputStream();
            dos = new DataOutputStream(outputStream);
            while (MainClient.flag) {

                myText = in.nextLine();
                myText = myText.trim();

                if (!myText.equals("")) {
                    if (myText.equals("exit")) {
                        MainClient.flag = false;
                    } else {
                        //System.out.println("write to server " + myText);
                        dos.writeUTF(userName + ": " + myText);
                        //dos.flush();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert dos != null;
            try {
                dos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
