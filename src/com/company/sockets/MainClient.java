package com.company.sockets;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by heinr on 29.01.2017.
 */
public class MainClient extends Thread {

    private static final int PORT_NUMBER = 8888;

    public static Socket socket = null;
    public static boolean flag = true;

    public static void main(String[] args) throws IOException {

        DataOutputStream dos = null;
        DataInputStream dis = null;

        Scanner in = new Scanner(System.in);
        System.out.print("Введите Ваше имя: ");
        String name = in.nextLine();
        name = name.trim();

        if (name.length() > 0) {
            try {

                socket = new Socket("127.0.0.1", PORT_NUMBER);

                ClientReadFromServer clientReadFromServer = new ClientReadFromServer(socket);
                clientReadFromServer.start();

                ClientSendToServer clientSendToServer = new ClientSendToServer(socket, name);
                clientSendToServer.start();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
