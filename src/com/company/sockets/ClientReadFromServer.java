package com.company.sockets;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by heinr on 29.01.2017.
 */
public class ClientReadFromServer extends Thread {

    private Scanner in = new Scanner(System.in);
    private Socket socket;

    DataInputStream dis = null;
    String myText;

    ClientReadFromServer(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {

        try {
            InputStream inputStream = socket.getInputStream();
            dis = new DataInputStream(inputStream);
            while (MainClient.flag) {
                myText = dis.readUTF().trim();
                if (myText != null) {
                    System.out.println(myText);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert dis != null;
            try {
                dis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
