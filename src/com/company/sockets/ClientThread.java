package com.company.sockets;

import com.sun.corba.se.spi.activation.Server;

import java.io.*;
import java.net.Socket;

/**
 * Created by heinr on 02.02.2017.
 */
public class ClientThread extends Thread {

    private Socket socket;

    public ClientThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            //PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            //DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            DataInputStream in = new DataInputStream(socket.getInputStream());

            while (true) {
                System.out.println("read line");
                String line = in.readUTF();
                System.out.println("read line after");

                for (Socket s : MainServer.clients) {
                    if (s != socket) {

                        System.out.println("text at the server " + line);
                        DataOutputStream other = new DataOutputStream(s.getOutputStream());
                        other.writeUTF(line);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Client disconnected");
        }
    }
}
